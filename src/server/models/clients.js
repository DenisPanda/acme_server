const mongoose = require('mongoose');

var clientSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  products: [{ product_name: { type: String, required: true }, product_description: { type: String, required: true } }]
});

mongoose.model('Client', clientSchema);
