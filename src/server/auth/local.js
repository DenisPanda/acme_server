//const moment = require('moment');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Admin = mongoose.model('Admin');

function encodeToken(user) {

  const playload = {
    username: user.username,
    id: user._id
  };

  return jwt.sign(playload, process.env.TOKEN_SECRET, { expiresIn: '7 days' });
}

function decodeToken(token, callback) {
  jwt.verify(token, process.env.TOKEN_SECRET, function (err, decoded) {
    if (err) {
      callback(err);
    } else {
      callback(null, decoded);
    }
  });
}

module.exports = {
  encodeToken,
  decodeToken
};
