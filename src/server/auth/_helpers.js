const bcrypt = require('bcryptjs');
const localAuth = require('./local');
const mongoose = require('mongoose');
const toObjectId = mongoose.Types.ObjectId;
const Admin = mongoose.model('Admin');
const Client = mongoose.model('Client');

function createUser(req, res) {
  let user = new Admin();
  user.name = req.body.name;
  user.username = req.body.username;
  user.setPassword(req.body.password);

  user.save((err) => {
    let token;
    token = user.generateJwt();
    res.status(200);
    res.json({
      token: token
    });
  });
}

function getUser(username) {
  return Admin.findOne({ username: username }, function(err, user) {
    if (err) { return err; }

    if (!user) {
      return {
        message: 'User not found'
      };
    }
  });
}

function comparePass(userPassword, databasePassword) {
  return bcrypt.compareSync(userPassword, databasePassword);
}

function ensureAuthenticated(req, res, next) {
  if (!(req.headers && req.headers.authorization)) {
    return res.status(400).json({
      status: 'Please log in'
    });
  }
  // decode the token
  const header = req.headers.authorization.split(' ');
  const token = header[1];
  localAuth.decodeToken(token, (err, payload) => {
    if (err) {
      return res.status(401).json({
        status: 'Token has expired'
      });
    } else {
      return Admin.findOne({ username: payload.username }, function(err, user) {
        if (err) {
          throw err;
        } else {
          return user;
        }
      })
      .then((user) => {
        next();
      })
      .catch((err) => {
        res.status(500).json({
          status: 'error'
        });
      });
    }
  });
}

function createClient(req, res) {
  let client = new Client(),
    data = req.body;
  client.name = data.name;
  client.description = data.description;

  client.save((err, cli) => {
    if (err) {
      return res.status(500).json({
        status: err
      });
    }
    res.status(200).json({
      id: cli._id,
      status: 'success'
    });
  });
}

function deleteClient(req, res) {
  const clientId = req.params.id;

  Client.remove({ _id: toObjectId(clientId) }, function(err, result) {
    if (err) {
      res.status(500).json({
        status: err
      });
    }
    res.status(202).json({
      status: 'success'
    });
  });
}

function updateClient(req, res) {
  const data = {
    name: req.body.name,
    description: req.body.description
  },
    id = req.params.id;

  Client.update({ _id: toObjectId(id) }, data, function(err, result) {
    if (err) {
      res.status(500).json({
        status: err
      });
    }
    res.status(200).json({
      status: 'success'
    });
  });
}

function createProduct(req, res) {
  const name = req.body.product_name,
    description = req.body.product_description;
  Client.findByIdAndUpdate(
    { _id: req.params.id },
    { $push:
      { products:
        { product_name: name,
          product_description: description
        }
    }
  },
    (err) => {
      if (err) {
        res.status(500).json({
          status: err
        });
      }
      res.status(200).json({
        status: 'success'
      });
    }
  );
}

function updateProduct(req, res) {
  const clientId = req.params.id,
    name = req.params.name,
    newName = req.body.product_name,
    newDescription = req.body.product_description;

  Client.findOneAndUpdate({ _id: toObjectId(clientId), 'products.product_name': name },
    { $set: { 'products.$.product_description': newDescription, 'products.$.product_name': newName } },
    function (err, numChanged) {
      if (err) {
        res.status(500).json({
          status: err
        });
      }
      res.status(200).json({
        status: 'success'
      });
    }
  );
}

function deleteProduct(req, res) {
  const clientId = req.params.id,
    name = req.params.name;

  Client.findOneAndUpdate({ _id: toObjectId(clientId), 'products.product_name': name },
    { $pull: { products: { product_name: name } } },
    (err) => {
      if (err) {
        res.status(500).json({
          status: err
        });
      }

      res.status(200).json({
        status: 'success'
      });
    }
  );
}

module.exports = {
  createUser,
  getUser,
  comparePass,
  ensureAuthenticated,
  createClient,
  updateClient,
  deleteClient,
  createProduct,
  updateProduct,
  deleteProduct
};
