const express = require('express');
const router = express.Router();
const localAuth = require('../auth/local');
const authHelpers = require('../auth/_helpers');

router.post('/register', (req, res, next)  => {
  return authHelpers.createUser(req, res);
});

router.post('/login', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  return authHelpers.getUser(username)
  .then((response) => {
    authHelpers.comparePass(password, response.hash);
    return response;
  })
  .then((response) => { return localAuth.encodeToken(response); })
  .then((token) => {
    res.status(200).json({
      status: 'success',
      token: token
    });
  })
  .catch((err) => {
    res.status(500).json({
      status: err
    });
  });
});

router.get('/user',
  authHelpers.ensureAuthenticated,
  (req, res, next)  => {
  res.status(200).json({
    status: 'success'
  });
});

router.post('/client',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    return authHelpers.createClient(req, res);
  }
);

router.patch('/client/:id',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    authHelpers.updateClient(req, res);
  }
);

router.delete('/client/:id',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    authHelpers.deleteClient(req, res);
  }
);

router.post('/client/:id/product',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    authHelpers.createProduct(req, res);
  }
);

router.patch('/client/:id/product/:name',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    authHelpers.updateProduct(req, res);
  }
);

router.delete('/client/:id/product/:name',
  authHelpers.ensureAuthenticated,
  (req, res, next) => {
    authHelpers.deleteProduct(req, res);
  }
);

module.exports = router;
