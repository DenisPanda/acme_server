const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Client = mongoose.model('Client');

router.get('/', function (req, res, next) {
  Client.find({}, (err, clients) => {
    if (err) {
      res.status(500).json({
        status: err
      });
    }
    if (!clients) {
      res.status(204);
    }
    res.status(200).json(clients);
  });
});

module.exports = router;
